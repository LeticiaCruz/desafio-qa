#language: pt
#author: Letícia Alavarces Cruz
#versao: v1.0
#encoding: iso-8859-1 

@Whatsapp
@iPhone

Funcionalidade: Criar lista de transmissao

-Narrativa
Sera criado uma lista de transmissao para enviar mensagens para várias pessoas ao mesmo tempo atraves do aplicativo

-Fora do escopo

Contexto: 
	Dado que o aplicativo whatsapp esteja aberto

Cenario: Criar lista de transmissao atraves do aplicativo

Dado que possua contatos na lista de contatos
Quando clicar em Listas de Transmissao
E selecionar os contatos
# Deve ser selecionado no minimo 2 contatos da lista para que seja possivel a criacao
E digitar a mensagem desejada
Entao a mensagem e enviada com sucesso para todos os contatos selecionados anterioramente