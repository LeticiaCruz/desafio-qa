#language: pt
#author: Letícia Alavarces Cruz
#versao: v1.0
#encoding: iso-8859-1 

@Whatsapp
@Web

Funcionalidade: Arquivar conversa atraves do whatsapp web

-Narrativa
Sera realizado o arquivamento de conversa atraves do whatsapp web

-Fora do escopo

Contexto: 
	Dado que o whatsapp web esteja aberto

Cenario: Arquivar conversas do whatsapp web com sucesso

	Dado que possua conversa a ser arquivada
	# Conversa de contato particular ou grupo
	Quando clicar no icone de seta na conversa
	E clicar em Arquivar conversa
	Entao o sistema remove a conversa da lista arquivando a mesma

